﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
   static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if(int.TryParse(source, out int number))
            {
                return number;
            }
            else
            {
                throw new ArgumentException("The number must be positive");
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            for(int i = 0; i < Math.Abs(num2); i++)
            {
                result += num1;
            }
            if (num2 < 0)
            {
                return -result;
            }
            return result;
        }

    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            
            bool usl;
            try
            {
                result = int.Parse(input);
                if(result < 0)
                {
                    throw new FormatException();
                }
                usl = true;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                return TryParseNaturalNumber(input, out result);
            }
            return usl;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> listEvenNumber = new List<int>();
            int i = 0;
            while (true)
            {
                if(i % 2 == 0)
                {
                    listEvenNumber.Add(i);
                    naturalNumber--;
                }
                if(naturalNumber <= 0)
                {
                    break;
                }
            }
            return listEvenNumber;
        }
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool usl;
            try
            {
                result = int.Parse(input);
                if (result < 0)
                {
                    throw new FormatException();
                }
                usl = true;
            }
            catch (FormatException ex)
            {
                ShowMessage(ex.Message);
                return TryParseNaturalNumber(input, out result);
            }
            return usl;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            int additionalNumber;
            int result = 0;
            for(int i = 1; source != 0; source /= 10, i++)
            {
                additionalNumber = source % 10;
                if(additionalNumber != digitToRemove)
                {
                    result = additionalNumber * Convert.ToInt32(Math.Pow(10,i)) + result;
                }
            }
            return result.ToString();
        }
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
